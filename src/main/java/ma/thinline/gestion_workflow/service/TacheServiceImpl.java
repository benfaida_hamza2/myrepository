package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dao.TacheRepository;
import ma.thinline.gestion_workflow.dao.WorkflowRepository;
import ma.thinline.gestion_workflow.dto.EtatDto;
import ma.thinline.gestion_workflow.dto.TacheDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;
import ma.thinline.gestion_workflow.mapper.EtatMapper;
import ma.thinline.gestion_workflow.mapper.TacheMapper;
import ma.thinline.gestion_workflow.mapper.UtilisateurMapper;
import ma.thinline.gestion_workflow.mapper.WorkflowMapper;
import ma.thinline.gestion_workflow.modele.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TacheServiceImpl implements ITacheService{

    private final TacheMapper tacheMapper;
    private final TacheRepository tacheRepository;
    private final EtatMapper etatMapper;
    private final IEtatService etatService;
    private final IWorkflowService workflowService;
    private final WorkflowMapper workflowMapper;
    private final UtilisateurMapper utilisateurMapper;
    private final IUserService userService;
    private final WorkflowRepository workflowRepository;

    public TacheServiceImpl(WorkflowRepository workflowRepository, WorkflowMapper workflowMapper, IUserService userService,UtilisateurMapper utilisateurMapper,IWorkflowService workflowService,IEtatService etatService,TacheMapper tacheMapper,TacheRepository tacheRepository,EtatMapper etatMapper){
        this.tacheMapper=tacheMapper;
        this.tacheRepository=tacheRepository;
        this.etatMapper=etatMapper;
        this.etatService=etatService;
        this.workflowService=workflowService;
        this.userService=userService;
        this.utilisateurMapper=utilisateurMapper;
        this.workflowMapper=workflowMapper;
        this.workflowRepository=workflowRepository;
    }

    @Override
    public List<TacheDto> getAllTaches(){ return tacheMapper.toDto(tacheRepository.findAll());}

    @Override
    public void saveTache(TacheDto dto,Long id,String cin) {
        Tache entity = tacheMapper.toEntity(dto);
        entity.setDate_modification(new Date());
        Etat etat = etatMapper.toEntity(etatService.getEtatByStatut("EN ATTENTE"));
        entity.setEtat(etat);
        Utilisateur entity1 = utilisateurMapper.toEntity(userService.findByCin(cin));
        entity.setUser(entity1);
        Workflow workflow=workflowMapper.toEntity(workflowService.getWorkflowById(id));
        entity.setOrdre(workflow.getTaches().size()+1);
        entity.setWorkflow(workflow);
        tacheRepository.save(entity);
    }

    @Override
    public TacheDto getTacheByTitre(String titre){return tacheMapper.toDto(tacheRepository.findAllByTitre(titre));}

    @Override
    public TacheDto getTacheById(Long id){
        boolean trouve = tacheRepository.existsById(id);
        if (!trouve)
            return null;
        return tacheMapper.toDto(tacheRepository.getOne(id));
    }

    @Override
    public void DeleteTache (Long id){
        Tache entity=tacheRepository.getById(id);
        int ordre=entity.getOrdre();
        Long workflow_id=entity.getWorkflow().getWorkflow_id();
        tacheRepository.deleteById(id);
        for(Tache tache :workflowRepository.getById(workflow_id).getTaches()){
            if(tache.getOrdre()>ordre)
            {
                tache.setOrdre(tache.getOrdre()-1);
                tacheRepository.save(tache);
            }
        }
    }

    @Override
    public void updateTache(Long id, TacheDto dto,String cin){
        Tache entity=tacheRepository.getById(id);
        entity.setDate_modification(new Date());
        entity.setTitre(dto.getTitre());
        entity.setUser(utilisateurMapper.toEntity(userService.findByCin(cin)));
        tacheRepository.save(entity);
    }
    @Override
    public String getResponsable(Long id){
        Tache tache=tacheRepository.getById(id);
        return tache.getUser().getFullname();
    }
    @Override
    public String getCin(Long id){
        Tache tache=tacheRepository.getById(id);
        return tache.getUser().getCin();
    }
    @Override
    public WorkflowDto getWorkflow(Long id){
        return  workflowMapper.toDto(tacheRepository.getById(id).getWorkflow());
    }
    @Override
    public void updateStatut(Long tache_id,String statut,String commentaire){

        Tache tache=tacheRepository.getById(tache_id);
        int ordre=tache.getOrdre();
        Etat etat=etatMapper.toEntity(etatService.getEtatByStatut(statut));
        if(statut.equals("REFUSE")){

             if(tache!=null){

                  if(etat!=null){
                      EtatDto etatDto1=etatService.getEtatByStatut("ANNULE");
                      tache.setEtat(etat);
                      tache.getWorkflow().setEtat(etat);
                      tache.setCommentaire(commentaire);
                      tache.setDate_modification(new Date());
                      tacheRepository.save(tache);
                      tache.getWorkflow().setCommentaire(commentaire);
                      int size=tache.getWorkflow().getTaches().size();
                      if(size==tache.getOrdre()){
                          tache.getWorkflow().setEtat(etat);
                      }
                      tache.getWorkflow().setUpdateTime(new Date());
                      workflowRepository.save(tache.getWorkflow());
                      Workflow workflow=tache.getWorkflow();
                      for (Tache r : workflow.getTaches()) {
                          if (r.getOrdre() > tache.getOrdre()){
                              r.setEtat(etatMapper.toEntity(etatDto1));
                              r.setDate_modification(new Date());
                              tacheRepository.save(r);
                          }
                      }
                  }
              }

        }
        else if(statut.equals("VALIDE")){

            if(tache!=null){


                if(etat!=null){
                    EtatDto etatDto1=etatService.getEtatByStatut("EN COURS");
                    tache.setEtat(etat);
                    tache.setCommentaire(commentaire);
                    tache.setDate_modification(new Date());
                    tacheRepository.save(tache);
                    Workflow workflow=tache.getWorkflow();
                    int size=workflow.getTaches().size();
                    if(size==tache.getOrdre()){
                        tache.getWorkflow().setEtat(etat);
                    }
                    else if(size>tache.getOrdre()){
                        for (Tache r : workflow.getTaches()) {
                            if (r.getOrdre() == tache.getOrdre()+1) {
                                r.setEtat(etatMapper.toEntity(etatDto1));
                                r.setDate_modification(new Date());
                                tacheRepository.save(r);
                            }
                        }
                    }
                    tache.getWorkflow().setUpdateTime(new Date());
                    workflowRepository.save(tache.getWorkflow());

                }
            }
        }
    }

}

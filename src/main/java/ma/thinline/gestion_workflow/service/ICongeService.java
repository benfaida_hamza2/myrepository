package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.CongeDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;

import java.util.List;

public interface ICongeService {

    List<CongeDto>getAllConge();

    CongeDto getCongeById(Long id);

    Long saveConge(CongeDto dto,String cin);

    void updateConge(Long id, CongeDto dto);

}

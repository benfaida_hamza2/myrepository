package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.TypeDto;

import java.util.List;

public interface ITypeService {

    List<TypeDto> getAllType();

    TypeDto getTypeByName(String type);

    TypeDto getTypeById(Long id);

    void saveType(TypeDto typeDto);

    void updateType(Long id, TypeDto dto);

    void deleteType(Long id);
}

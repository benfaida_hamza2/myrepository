package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.EtatDto;

import java.util.List;

public interface IEtatService {

    List<EtatDto> getAllEtats();

    EtatDto getEtatByStatut(String statut);

    EtatDto getEtatById(Long id);

    void saveEtat(EtatDto etatDto);

    void updateEtat(Long id, EtatDto dto);

    void deleteEtat(Long id);
}

package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dao.CongeRepository;
import ma.thinline.gestion_workflow.dto.CongeDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;
import ma.thinline.gestion_workflow.mapper.*;
import ma.thinline.gestion_workflow.modele.*;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CongeServiceImp implements ICongeService{

    private final CongeRepository congeRepository;
    private final CongeMapper congeMapper;
    private final EtatMapper etatMapper;
    private final IEtatService etatService;
    private final ITypeService typeService;
    private final TypeMapper typeMapper;
    private final IUserService userService;
    private final IWorkflowService workflowService;
    private final TacheMapper tacheMapper;
    private final UtilisateurMapper utilisateurMapper;

    public CongeServiceImp(TacheMapper tacheMapper,UtilisateurMapper utilisateurMapper,IWorkflowService workflowService,IUserService userService,TypeMapper typeMapper,ITypeService typeService,EtatMapper etatMapper,IEtatService etatService,CongeRepository congeRepository,CongeMapper congeMapper){
        this.congeMapper=congeMapper;
        this.congeRepository=congeRepository;
        this.etatMapper=etatMapper;
        this.etatService=etatService;
        this.typeService=typeService;
        this.typeMapper=typeMapper;
        this.utilisateurMapper=utilisateurMapper;
        this.userService=userService;
        this.workflowService=workflowService;
        this.tacheMapper=tacheMapper;
    }

    @Override
    public List<CongeDto> getAllConge() {return congeMapper.toDto(congeRepository.findAll());}

    @Override
    public CongeDto getCongeById(Long id){return congeMapper.toDto(congeRepository.getById(id));}

    @Override
    public Long saveConge(CongeDto dto,String cin){
        Long id;
        Conge entity=congeMapper.toEntity(dto);
        entity.setUpdateTime(new Date());
        entity.setCreatedTime(new Date());
        Etat etat= etatMapper.toEntity(etatService.getEtatByStatut("EN ATTENTE"));
        entity.setEtat(etat);
        Type type= typeMapper.toEntity(typeService.getTypeByName("Demande de congés"));
        entity.setType(type);
        congeRepository.save(entity);
        id=workflowService.getByMaxId().getWorkflow_id();
        userService.addWorkflowToUser(dto.getCreateur(),id);
        System.out.println(cin);
        if(cin!=null)
        {
            Utilisateur entity1=utilisateurMapper.toEntity(userService.findByCin(cin));
            if(entity1!=null)
            userService.addWorkflowToUser(entity1.getFullname(),workflowService.getByMaxId().getWorkflow_id());
        }
        return id;
    }

    @Override
    public void updateConge(Long id, CongeDto dto){
        Conge entity=congeRepository.getById(id);
        entity.setTitre(dto.getTitre());
        entity.setTaches(tacheMapper.toEntity(dto.getTaches()));
        entity.setInfo(dto.getInfo());
        entity.setDateDebut(dto.getDateDebut());
        entity.setDateFin(dto.getDateFin());
        entity.setReprise(dto.getReprise());
        entity.setUpdateTime(new Date());
        congeRepository.save(entity);
    }

}

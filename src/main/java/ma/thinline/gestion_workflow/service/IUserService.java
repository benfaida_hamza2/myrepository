package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.UtilisateurDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface IUserService extends UserDetailsService {

    UtilisateurDto findByUsername(String email);

    UtilisateurDto findByFullname(String fullname);

    UtilisateurDto findByCin(String cin);

    List<UtilisateurDto> getAllUsers();

    UtilisateurDto getUserById(Long id);

    void saveUser(UtilisateurDto utilisateurDto);

    void updateUser(Long id, UtilisateurDto dto);

    void userUpdate(Long id, UtilisateurDto dto);
    
    void deleteUser(Long id);

    void addRoleToUser(String username,String role);

    void addWorkflowToUser(String username, Long id);

    void addTacheToUser(String username, Long id);

    void deleteWorkflowToUser(String username,Long id);

    void deleteWorkflowToUsers(Long id);

    Boolean verifyPassword(UtilisateurDto dto);

}

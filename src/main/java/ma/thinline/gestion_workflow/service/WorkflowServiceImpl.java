package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dao.TacheRepository;
import ma.thinline.gestion_workflow.dao.WorkflowRepository;
import ma.thinline.gestion_workflow.dto.RoleDto;
import ma.thinline.gestion_workflow.dto.TacheDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;
import ma.thinline.gestion_workflow.mapper.*;
import ma.thinline.gestion_workflow.modele.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class WorkflowServiceImpl implements IWorkflowService{

    private final WorkflowRepository workflowRepository;
    private final WorkflowMapper workflowMapper;
    private final IEtatService etatService;
    private final EtatMapper etatMapper;
    private final TacheMapper tacheMapper;
    private final TypeMapper typeMapper;
    private final ITypeService typeService;
    private final IUserService userService;
    private final UtilisateurMapper utilisateurMapper;
    private final TacheRepository tacheRepository;

    public WorkflowServiceImpl(TacheRepository tacheRepository,UtilisateurMapper utilisateurMapper,IUserService userService,ITypeService typeService,TypeMapper typeMapper,EtatMapper etatMapper,IEtatService etatService,WorkflowMapper workflowMapper,WorkflowRepository workflowRepository,TacheMapper tacheMapper) {
       this.workflowMapper=workflowMapper;
       this.tacheMapper=tacheMapper;
       this.workflowRepository=workflowRepository;
       this.etatService = etatService;
       this.etatMapper=etatMapper;
       this.typeMapper=typeMapper;
       this.typeService=typeService;
       this.userService=userService;
       this.utilisateurMapper=utilisateurMapper;
       this.tacheRepository=tacheRepository;
    }

    @Override
    public List<WorkflowDto> getAllWorkflows() {return workflowMapper.toDto(workflowRepository.findAll());}

    @Override
    public Long saveWorkflow(WorkflowDto dto,String cin){
        Long id;
        Workflow entity=workflowMapper.toEntity(dto);
        entity.setUpdateTime(new Date());
        entity.setCreatedTime(new Date());
        Etat etat= etatMapper.toEntity(etatService.getEtatByStatut("EN ATTENTE"));
        entity.setEtat(etat);
        Type type= typeMapper.toEntity(typeService.getTypeByName("Réclamation"));
        entity.setType(type);
        workflowRepository.save(entity);
        id=this.getByMaxId().getWorkflow_id();
        userService.addWorkflowToUser(dto.getCreateur(),id);
        if(cin!=null)
        {
            Utilisateur entity1=utilisateurMapper.toEntity(userService.findByCin(cin));
            if(entity1!=null)
                userService.addWorkflowToUser(entity1.getFullname(),this.getByMaxId().getWorkflow_id());
        }
        return id;
    }

    @Override
    public List<WorkflowDto> getWorkflowByStatut(String statut)
    {
        Etat etat=etatMapper.toEntity(etatService.getEtatByStatut(statut));
        return workflowMapper.toDto(workflowRepository.findWorkflowByEtat(etat));
    }

    @Override
    public List<WorkflowDto> getWorkflowByType(String type)
    {
        return workflowMapper.toDto(workflowRepository.findAllByType(type));
    }

    @Override
    public void updateWorkflow(Long id, WorkflowDto dto){
        Workflow entity=workflowRepository.getById(id);
        entity.setTitre(dto.getTitre());
        entity.setTaches(tacheMapper.toEntity(dto.getTaches()));
        entity.setInfo(dto.getInfo());
        entity.setCommentaire(dto.getCommentaire());
        entity.setUpdateTime(new Date());
        workflowRepository.save(entity);
    }

    @Override
    public boolean updateStatut(Long id, String statut){
        Workflow entity=workflowRepository.getById(id);
            entity.setUpdateTime(new Date());
            if(!entity.getTaches().isEmpty()){
                Etat etat = etatMapper.toEntity(etatService.getEtatByStatut(statut));
                entity.setEtat(etat);
                entity.setUpdateTime(new Date());
            if (statut.equals("EN COURS")) {
                for (Tache r : entity.getTaches()) {
                    if (r.getOrdre() == 1) {
                        r.setEtat(etat);
                        r.setDate_modification(new Date());
                        tacheRepository.save(r);
                        break;
                    }
                }
                workflowRepository.save(entity);
                return true;
            }
            else if (statut.equals("ANNULE")) {
                for (Tache r : entity.getTaches()) {
                    if (r.getEtat().getStatut().equals("EN COURS") || r.getEtat().getStatut().equals("EN ATTENTE")) {
                        r.setEtat(etat);
                        r.setDate_modification(new Date());
                        tacheRepository.save(r);
                    }
                }
                workflowRepository.save(entity);
                return true;
            }
        }
            return false;
    }

    @Override
    public WorkflowDto getByMaxId(){
        return workflowMapper.toDto(workflowRepository.findWorkflowByMaxWorkflow_id());
    }

    @Override
    public void deleteWorkflow(Long id){workflowRepository.delete(workflowRepository.getById(id));}

    @Override
    public  WorkflowDto getWorkflowById(Long id){
        boolean trouve = workflowRepository.existsById(id);
        if (!trouve)
            return null;
        return workflowMapper.toDto(workflowRepository.getById(id));
    }

    @Override
    public void updateCommentaire(Long id,String motif){
        Workflow entity=workflowRepository.getById(id);
        entity.setCommentaire(motif);
        entity.setUpdateTime(new Date());
        workflowRepository.save(entity);
    }


}

package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dao.TypeRepository;
import ma.thinline.gestion_workflow.dto.TypeDto;
import ma.thinline.gestion_workflow.mapper.TypeMapper;
import ma.thinline.gestion_workflow.modele.Type;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TypeServiceImpl implements ITypeService, CommandLineRunner {

    private final TypeMapper typeMapper;
    private final TypeRepository typeRepository;

    public TypeServiceImpl( TypeMapper typeMapper,TypeRepository typeRepository) {
        this.typeRepository=typeRepository;
        this.typeMapper=typeMapper;
    }

    @Override
    public List<TypeDto> getAllType() {
        return typeMapper.toDto(typeRepository.findAll());
    }

    @Override
    public TypeDto getTypeByName(String type) {
        return typeMapper.toDto(typeRepository.findByType(type));
    }

    @Override
    public void saveType (TypeDto dto) {
        typeRepository.save(typeMapper.toEntity(dto));
    }

    @Override
    public TypeDto getTypeById(Long id){
        boolean trouve = typeRepository.existsById(id);
        if (!trouve)
            return null;
        return typeMapper.toDto(typeRepository.getOne(id));
    }

    @Override
    public void deleteType(Long id){ typeRepository.deleteById(id);}

    @Override
    public  void updateType(Long id, TypeDto dto){
        Type entity=typeRepository.getById(id);
        entity.setType(dto.getType());
        typeRepository.save(entity);
    }

    @Override
    public void run(String... args) throws Exception {
    /*
        Type type1=new Type();
        Type type2=new Type();
        type1.setType("Demande de congés");
        type2.setType("Réclamation");
        typeRepository.save(type1);
        typeRepository.save(type2);

     */

    }
}

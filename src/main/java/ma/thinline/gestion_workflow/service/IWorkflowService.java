package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.WorkflowDto;

import java.util.List;

public interface IWorkflowService {

    List<WorkflowDto> getAllWorkflows();

    Long saveWorkflow(WorkflowDto dto,String cin);

    List<WorkflowDto> getWorkflowByStatut(String statut);

    List<WorkflowDto> getWorkflowByType(String type);

    WorkflowDto getWorkflowById(Long id);

    WorkflowDto getByMaxId();

    void updateCommentaire(Long id,String motif);

    void updateWorkflow(Long id, WorkflowDto dto);

    boolean updateStatut(Long id, String statut);

    void deleteWorkflow(Long id);

}

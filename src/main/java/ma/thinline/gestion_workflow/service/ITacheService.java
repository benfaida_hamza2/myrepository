package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dto.TacheDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;

import java.util.List;

public interface ITacheService {

    List<TacheDto> getAllTaches();

    void saveTache(TacheDto dto,Long id,String cin);

    TacheDto getTacheByTitre(String titre);

    TacheDto getTacheById(Long id);

    void updateTache(Long id, TacheDto dto,String cin);

    void DeleteTache (Long id);

    String getResponsable(Long id);

    String getCin(Long id);

    WorkflowDto getWorkflow(Long id);

    void updateStatut(Long tache_id,String statut,String commentaire);

}

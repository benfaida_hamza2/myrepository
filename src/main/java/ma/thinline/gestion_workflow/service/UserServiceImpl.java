package ma.thinline.gestion_workflow.service;

import ma.thinline.gestion_workflow.dao.TacheRepository;
import ma.thinline.gestion_workflow.dao.UtilisateurRepository;
import ma.thinline.gestion_workflow.dao.WorkflowRepository;
import ma.thinline.gestion_workflow.dto.RoleDto;
import ma.thinline.gestion_workflow.dto.UtilisateurDto;
import ma.thinline.gestion_workflow.mapper.RoleMapper;
import ma.thinline.gestion_workflow.mapper.UtilisateurMapper;
import ma.thinline.gestion_workflow.mapper.WorkflowMapper;
import ma.thinline.gestion_workflow.modele.Role;
import ma.thinline.gestion_workflow.modele.Utilisateur;
import ma.thinline.gestion_workflow.modele.Workflow;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements IUserService{

    private final UtilisateurRepository userRepository;
    private final UtilisateurMapper utilisateurMapper;
    private final WorkflowRepository workflowRepository;
    private final RoleMapper roleMapper;
    private final IRoleService roleService;
    private final WorkflowMapper workflowMapper;
    private final PasswordEncoder passwordEncoder;
    private final TacheRepository tacheRepository;

    public UserServiceImpl(TacheRepository tacheRepository,WorkflowMapper workflowMapper,WorkflowRepository workflowRepository,UtilisateurRepository userRepository, UtilisateurMapper utilisateurMapper,RoleMapper roleMapper,IRoleService roleService,PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.utilisateurMapper = utilisateurMapper;
        this.roleMapper = roleMapper;
        this.roleService=roleService;
        this.passwordEncoder=passwordEncoder;
        this.workflowRepository=workflowRepository;
        this.workflowMapper=workflowMapper;
        this.tacheRepository=tacheRepository;
    }

    @Override
    public void saveUser(UtilisateurDto dto) {
        Utilisateur entity = utilisateurMapper.toEntity(dto);
        List<Role> rolesPersist = new ArrayList<>();
        if(entity.getRoles()!=null) {
            for (Role role : entity.getRoles()) {
                Role userRole = roleMapper.toEntity(roleService.getRoleByName(role.getRole()));
                rolesPersist.add(userRole);
            }

            entity.setRoles(rolesPersist);
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
    }

    @Override
    public UtilisateurDto findByUsername(String Username) {
        return utilisateurMapper.toDto(userRepository.findByUsername(Username));
    }

    @Override
    public UtilisateurDto findByFullname(String fullname) {
        return utilisateurMapper.toDto(userRepository.findByFullname(fullname));
    }

    @Override
    public UtilisateurDto findByCin(String cin) {
        return utilisateurMapper.toDto(userRepository.findByCin(cin));
    }

    @Override
    public List<UtilisateurDto> getAllUsers() {
        return utilisateurMapper.toDto(userRepository.findAll());
    }

    @Override
    public UtilisateurDto getUserById(Long id) {
        boolean trouve = userRepository.existsById(id);
        if (!trouve)
            return null;
        return utilisateurMapper.toDto(userRepository.getOne(id));
    }

    @Override
    public void userUpdate(Long id, UtilisateurDto dto)
    {
        Utilisateur entity=userRepository.getById(id);
        entity.setUsername(dto.getUsername());
        entity.setFullname(dto.getFullname());
        entity.setCin(dto.getCin());
        entity.setPassword(passwordEncoder.encode(dto.getPassword()));
        userRepository.save(entity);
    }

    @Override
    public void updateUser(Long id, UtilisateurDto dto)
    {
        Utilisateur entity=userRepository.getById(id);
        entity.setUsername(dto.getUsername());
        entity.setFullname(dto.getFullname());
        entity.setCin(dto.getCin());
        userRepository.save(entity);
    }

    @Override
    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

    @Override
    public void addRoleToUser(String username, String role){
        Utilisateur dto=userRepository.findByUsername(username);
        RoleDto roledto=roleService.getRoleByName(role);
        dto.getRoles().add(roleMapper.toEntity(roledto));
    }

    @Override
    public void addWorkflowToUser(String fullname, Long id){
        Utilisateur entity=userRepository.findByFullname(fullname);
        entity.getWorkflows().add(workflowRepository.getById(id));
    }
    @Override
    public void addTacheToUser(String cin, Long id){
        Utilisateur entity=userRepository.findByCin(cin);
        entity.getTaches().add(tacheRepository.getById(id));
    }
    @Override
    public void deleteWorkflowToUser(String username,Long id){
        Utilisateur entity=userRepository.findByUsername(username);
        entity.getWorkflows().remove(workflowRepository.getById(id));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{

        Utilisateur user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,getAuthorities(user.getRoles()));
    }

    @Override
    public Boolean verifyPassword(UtilisateurDto dto){
        Utilisateur  entity=userRepository.findByUsername(dto.getUsername());
        if(entity==null)
            return false;
        else {
            if (passwordEncoder.matches(dto.getPassword(),entity.getPassword()))
                return  true;
            return false;
        }
    }
    private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles) {
        List<GrantedAuthority> springSecurityAuthorities = new ArrayList<>();
        for (Role r : roles) {
            springSecurityAuthorities.add(new
                    SimpleGrantedAuthority(r.getRole()));
        }
        return springSecurityAuthorities;
    }

    @Override
    public void deleteWorkflowToUsers(Long id){
        List<Workflow> workflows;
        int num;
        List<Utilisateur> users= userRepository.findAll();
        for(Utilisateur user : users)
        {
            if(user.getWorkflows()!=null)
            {
                for(Workflow workflow : user.getWorkflows())
                {
                    if(workflow.getWorkflow_id()==id){
                        Utilisateur entity=userRepository.findByUsername(user.getUsername());
                        entity.getWorkflows().remove(workflowRepository.getById(id));
                        break;
                    }
                }
            }

        }
    }
}

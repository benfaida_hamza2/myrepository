package ma.thinline.gestion_workflow.configuration;

import ma.thinline.gestion_workflow.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserServiceImpl userService;

    @Override
    protected  void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeHttpRequests().anyRequest().permitAll();
        /*http.csrf().disable()
                  .authorizeRequests()
                  .antMatchers("/**")
                  .fullyAuthenticated().and().httpBasic();

         */

        /* http.authorizeRequests().
                antMatchers("/auth/login").permitAll().
                antMatchers("/Etat/**").hasAnyAuthority("CLIENT").
                antMatchers("/User/**").hasAnyAuthority("ADMIN").
                anyRequest().authenticated().
                and().csrf().disable().
                formLogin().permitAll();
                */
    }
}

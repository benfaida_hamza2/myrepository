package ma.thinline.gestion_workflow.controller;

import ma.thinline.gestion_workflow.dto.UtilisateurDto;
import ma.thinline.gestion_workflow.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/User")
@CrossOrigin("*")
public class UtilisateurController {

    private final IUserService userservice;

    public UtilisateurController(IUserService userservice)
    {
        this.userservice=userservice;
    }

    @GetMapping(value = "listusers")
    public ResponseEntity<Object> viewUsers() {
        return new ResponseEntity<>(userservice.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "username/{email}")
    public ResponseEntity<Object> getUserByUsername(@PathVariable("email") String email) {
        UtilisateurDto dto=userservice.findByUsername(email);
       if (dto==null)
            return new ResponseEntity<>("l'utilisateur n'existe pas", HttpStatus.OK);
        return new ResponseEntity<>(dto, HttpStatus.OK);

    }

    @GetMapping(value = "fullname/{fullname}")
    public ResponseEntity<Object> getUserByCin(@PathVariable("fullname") String fullname) {
        UtilisateurDto dto=userservice.findByFullname(fullname);
        if (dto==null)
            return new ResponseEntity<>("l'utilisateur n'existe pas", HttpStatus.OK);
        return new ResponseEntity<>(dto, HttpStatus.OK);

    }

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable("id") Long id) {
        UtilisateurDto dto=userservice.getUserById(id);
        if (dto==null)
            return new ResponseEntity<>("l'utilisateur n'existe pas", HttpStatus.OK);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "Createuser")
    public ResponseEntity<Object> createUser(@Valid @RequestBody UtilisateurDto dto) {
        UtilisateurDto dto1=userservice.findByUsername(dto.getUsername());
        if (dto1==null){
            userservice.saveUser(dto);
            return new ResponseEntity<>("l'utilisateur a été crée", HttpStatus.CREATED);}
        return new ResponseEntity<>("email existe déja", HttpStatus.OK);
    }

    @PostMapping(value = "verifypassword")
    public ResponseEntity<Object> verifyPassword(@Valid @RequestBody UtilisateurDto dto) {
       if(userservice.verifyPassword(dto))
           return new ResponseEntity<>("les mots de passes sont identiques", HttpStatus.OK);
       else
           return new ResponseEntity<>("les mots de passes sont pas identiques", HttpStatus.FORBIDDEN);

    }

    @PutMapping(value="Edit/{id}")
    public ResponseEntity<Object> userUpdate(@PathVariable Long id,@Valid @RequestBody UtilisateurDto dto) {

        if (userservice.getUserById(id) == null)
            return new ResponseEntity<>("l'utilisateur n'existe pas", HttpStatus.FORBIDDEN);
        else{
            UtilisateurDto dto1=userservice.getUserById(dto.getUser_id());
            if(dto1!=null){
                if(dto.getPassword().equals("")) {
                    userservice.updateUser(id,dto);
                }
                else{
                    userservice.userUpdate(id,dto);
                }

                return new ResponseEntity<>("l'utilisateur a été modifié avec succés", HttpStatus.OK);
            }
            else
                return new ResponseEntity<>("l'utilisateur a été modifié avec succés", HttpStatus.BAD_REQUEST);


        }

    }
    @DeleteMapping(value ="Delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id){
        if (userservice.getUserById(id) == null)
            return new ResponseEntity<>("l'utilisateur n'existe pas", HttpStatus.FORBIDDEN);
        userservice.deleteUser(id);
        return new ResponseEntity<>("l'utilisateur a été supprimé avec succés", HttpStatus.OK);
    }

    @DeleteMapping(value ="Deletewor/{username}/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id, @PathVariable("username") String username){
        if (userservice.findByUsername(username) == null)
            return new ResponseEntity<>("échec", HttpStatus.FORBIDDEN);
        userservice.deleteWorkflowToUser(username,id);
        return new ResponseEntity<>("succés", HttpStatus.OK);
    }

    @DeleteMapping(value ="DeleteworAll/{id}")
    public ResponseEntity<Object> deleteUsers(@PathVariable("id") Long id){
        userservice.deleteWorkflowToUsers(id);
        return new ResponseEntity<>("succés", HttpStatus.OK);
    }

}

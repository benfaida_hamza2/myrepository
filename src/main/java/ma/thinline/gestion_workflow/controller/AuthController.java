package ma.thinline.gestion_workflow.controller;

import ma.thinline.gestion_workflow.dto.UtilisateurDto;
import ma.thinline.gestion_workflow.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin("*")
//@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private IUserService userservice;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @GetMapping("/bienvenue")
    public  String bienvenue(){
        return "Workflow thinline vous souhaite le bienvenue";
    }
    @PostMapping(value = "login")
    public ResponseEntity<Object> loginUser(@Valid @RequestBody UtilisateurDto dto){

        UtilisateurDto entity=userservice.findByUsername(dto.getUsername());
        if(entity==null)
            return new ResponseEntity<>("Username est incorrecte", HttpStatus.OK);
        else {
            if (passwordEncoder.matches(dto.getPassword(),entity.getPassword()))
                return new ResponseEntity<>(entity, HttpStatus.OK);
            else
                return new ResponseEntity<>("mot de passe est incorrecte", HttpStatus.OK);
        }
    }

    @PostMapping(value = "RegisterClient")
    public ResponseEntity<?> registerClient(@Valid @RequestBody UtilisateurDto dto) {
        UtilisateurDto dto1 = userservice.findByUsername(dto.getUsername());
        UtilisateurDto dto2 = userservice.findByCin(dto.getCin());
        if (dto1 == null && dto2== null){
            userservice.saveUser(dto);
            userservice.addRoleToUser(dto.getUsername(),"CLIENT");
            return new ResponseEntity<>("l'utilisateur a été crée", HttpStatus.CREATED);
        }
        else if(dto2==null && dto1 !=null)
           return new ResponseEntity<>("email existe déja", HttpStatus.FORBIDDEN);
        else if (dto1==null && dto2 !=null)
           return new ResponseEntity<>("cin existe déja", HttpStatus.BAD_REQUEST);
        else
           return new ResponseEntity<>("username et cin existe déja", HttpStatus.OK);

    }

    @PostMapping(value = "RegisterAdmin")
    public ResponseEntity<?> registerAdmin(@Valid @RequestBody UtilisateurDto dto) {
        UtilisateurDto dto1 = userservice.findByUsername(dto.getUsername());
        UtilisateurDto dto2 = userservice.findByCin(dto.getCin());
        if (dto1 == null && dto2 == null) {
            userservice.saveUser(dto);
            userservice.addRoleToUser(dto.getUsername(), "ADMIN");
            return new ResponseEntity<>("l'utilisateur a été crée", HttpStatus.CREATED);
        }
        else if(dto2==null && dto1 !=null)
            return new ResponseEntity<>("email existe déja", HttpStatus.FORBIDDEN);
        else if (dto1==null && dto2 !=null)
            return new ResponseEntity<>("cin existe déja", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>("username et cin existe déja", HttpStatus.OK);
    }
}

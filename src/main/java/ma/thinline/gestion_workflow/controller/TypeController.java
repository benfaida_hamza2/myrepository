package ma.thinline.gestion_workflow.controller;


import ma.thinline.gestion_workflow.dto.TypeDto;
import ma.thinline.gestion_workflow.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("TypeWorkflow")
@CrossOrigin("*")
public class TypeController {

    @Autowired
    private ITypeService typeService;

    @GetMapping(value = "listtypes")
    public ResponseEntity<Object> viewEtats() {
        List<TypeDto> list= typeService.getAllType();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value ="type/{type}")
    public ResponseEntity<Object> getEtatByStatut(@PathVariable String type){
        TypeDto dto= typeService.getTypeByName(type);
        if(dto==null)
            return new ResponseEntity<>("le type n'existe pas",HttpStatus.OK);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Object> getEtatById(@PathVariable Long id){
        TypeDto dto= typeService.getTypeById(id);
        if(dto==null)
            return new ResponseEntity<>("le type n'existe pas",HttpStatus.OK);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping(value = "createtype")
    public ResponseEntity<Object> CreateEtat(@RequestBody TypeDto dto){
        typeService.saveType(dto);
        return new ResponseEntity<>("le type a été créé avec succés",HttpStatus.CREATED);
    }

    @PutMapping(value = "edittype/{id}")
    public  ResponseEntity<Object> UpdateEtat(@PathVariable Long id, @RequestBody TypeDto dto){
        if(typeService.getTypeById(id)==null)
            return new ResponseEntity<>("le type n'existe pas",HttpStatus.OK);
        typeService.updateType(id,dto);
        return new ResponseEntity("le type a été modifié avec succés",HttpStatus.OK);
    }

    @DeleteMapping(value ="deletetype/{id}")
    public ResponseEntity<Object> DeleteEtat(@PathVariable Long id ){
        if(typeService.getTypeById(id)==null)
            return new ResponseEntity<>("le type n'existe pas",HttpStatus.OK);
        typeService.deleteType(id);
        return new ResponseEntity("le type a été supprimé avec succés",HttpStatus.OK);
    }

}

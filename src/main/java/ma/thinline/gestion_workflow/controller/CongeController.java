package ma.thinline.gestion_workflow.controller;

import ma.thinline.gestion_workflow.dto.CongeDto;
import ma.thinline.gestion_workflow.service.ICongeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("Conge")
@CrossOrigin("*")
public class CongeController {

    private final ICongeService congeService;

    public CongeController(ICongeService congeService){
        this.congeService=congeService;
    }

    @GetMapping(value = "/listconge")
    public ResponseEntity<Object> viewConge() {
        List<CongeDto> list=congeService.getAllConge();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<Object> getCongeById(@PathVariable Long id ){
        CongeDto dto=congeService.getCongeById(id);
        if(dto==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.OK);
        return new ResponseEntity<>(dto,HttpStatus.OK);

    }

    @PostMapping(value="/createconge/{cin}")
    public ResponseEntity<Object> createConge(@RequestBody CongeDto dto,@PathVariable String cin){
        Long id=congeService.saveConge(dto,cin);
        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @PostMapping(value="/createconge")
    public ResponseEntity<Object> createConge(@RequestBody CongeDto dto){
        Long id=congeService.saveConge(dto,null);
        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @PutMapping(value ="updateconge/{id}")
    public ResponseEntity<Object> updateConge(@PathVariable Long id,@RequestBody CongeDto dto ){
        if(congeService.getCongeById(id)==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.FORBIDDEN);
        congeService.updateConge(id,dto);
        return new ResponseEntity<>("le workflow a été modifié avec succés",HttpStatus.OK);
    }
}

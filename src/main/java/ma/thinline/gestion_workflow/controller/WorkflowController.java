package ma.thinline.gestion_workflow.controller;

import ma.thinline.gestion_workflow.dto.CongeDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;
import ma.thinline.gestion_workflow.service.IWorkflowService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("Workflow")
@CrossOrigin("*")
public class WorkflowController {

    private final IWorkflowService workflowService;

    public WorkflowController(IWorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    @GetMapping(value = "listworkflows")
    public ResponseEntity<Object> viewWorkflows() {
        List<WorkflowDto> list= workflowService.getAllWorkflows();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<Object> getWorkflowByid(@PathVariable Long id ){
       WorkflowDto dto=workflowService.getWorkflowById(id);
       if(dto==null)
           return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.OK);
       return new ResponseEntity<>(dto,HttpStatus.OK);

    }

    @GetMapping(value = "statut/{statut}")
    public ResponseEntity<Object> getByStatut(@PathVariable String statut)
    {
        List<WorkflowDto> dto=workflowService.getWorkflowByStatut(statut);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping(value = "type/{type}")
    public ResponseEntity<Object> getByType(@PathVariable String type)
    {
        List<WorkflowDto> dto=workflowService.getWorkflowByType(type);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping(value="/createworkflow")
    public ResponseEntity<Object> createWorkflow(@RequestBody WorkflowDto dto){
        Long id=workflowService.saveWorkflow(dto,null);
       return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @PostMapping(value="/createworkflow/{cin}")
    public ResponseEntity<Object> createWorkflow(@RequestBody CongeDto dto, @PathVariable String cin){
        Long id=workflowService.saveWorkflow(dto,cin);
        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }

    @PutMapping(value ="updateworkflow/{id}")
    public ResponseEntity<Object> updateWorkflow(@PathVariable Long id,@RequestBody WorkflowDto dto ){
        if(workflowService.getWorkflowById(id)==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.OK);
        workflowService.updateWorkflow(id,dto);
        return new ResponseEntity<>("le workflow a été modifié avec succés",HttpStatus.OK);
    }

    @PutMapping(value ="updatecommentaire/{id}")
    public ResponseEntity<Object> updateCommentaire(@PathVariable Long id,@RequestBody String comment ){
        if(workflowService.getWorkflowById(id)==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.OK);
        workflowService.updateCommentaire(id,comment);
        return new ResponseEntity<>("le workflow a été modifié avec succés",HttpStatus.OK);
    }

    @PutMapping(value ="updatestatut/{id}")
    public ResponseEntity<Object> updateWorkflow(@PathVariable Long id,@RequestBody String username ){
        if(workflowService.getWorkflowById(id)==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.NOT_FOUND);
        boolean var=workflowService.updateStatut(id,username);
        if(var==true)
        return new ResponseEntity<>("le workflow a été modifié avec succés",HttpStatus.OK);
        return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.FORBIDDEN);
    }

    @DeleteMapping(value ="deleteworkflow/{id}")
    public ResponseEntity<Object> deleteWorkflow(@PathVariable Long id ){
        if(workflowService.getWorkflowById(id)==null)
            return new ResponseEntity<>("le workflow n'existe pas",HttpStatus.OK);
       workflowService.deleteWorkflow(id);
       return ResponseEntity.ok("le workflow a été supprimé avec succés");
    }

}

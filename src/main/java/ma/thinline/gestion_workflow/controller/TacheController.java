package ma.thinline.gestion_workflow.controller;

import ma.thinline.gestion_workflow.dto.TacheDto;
import ma.thinline.gestion_workflow.dto.WorkflowDto;
import ma.thinline.gestion_workflow.service.ITacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("Tache")
@CrossOrigin("*")
public class TacheController {

    @Autowired
    private ITacheService tacheService;

    @GetMapping(value = "listtaches")
    public ResponseEntity<Object> ViewTaches() {
        List<TacheDto> list= tacheService.getAllTaches();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value ="titre/{titre}")
    public ResponseEntity<Object> getRoleByName(@PathVariable String titre){
        TacheDto dto= tacheService.getTacheByTitre(titre);
        if (dto==null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.OK);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Object> getRoleById(@PathVariable Long id){
        TacheDto dto= tacheService.getTacheById(id);
        if (dto==null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @GetMapping(value = "responsable/{id}")
    public ResponseEntity<String> getResponsable(@PathVariable Long id){
        String name=tacheService.getResponsable(id);
        if (name==null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        return new ResponseEntity<>(name, HttpStatus.OK);
    }

    @GetMapping(value = "cin/{id}")
    public ResponseEntity<String> getCin(@PathVariable Long id){
        String cin=tacheService.getCin(id);
        if (cin==null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        return new ResponseEntity<>(cin, HttpStatus.OK);
    }

    @GetMapping(value = "workflow/{id}")
    public ResponseEntity<Object> getWorkflow(@PathVariable Long id){
        WorkflowDto dto =tacheService.getWorkflow(id);
        if (dto==null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @PostMapping(value = "createtache/{id}/{cin}")
    public ResponseEntity<String> createRole(@Valid @RequestBody TacheDto dto,@PathVariable Long id,@PathVariable String cin){
        tacheService.saveTache(dto,id,cin);
        return new ResponseEntity<>("la tache a été crée avec succés", HttpStatus.CREATED);
    }

    @PutMapping(value = "edittache/{id}/{cin}")
    public  ResponseEntity<String> updateRole(@PathVariable Long id,@Valid @RequestBody TacheDto dto,@PathVariable String cin){
        if (tacheService.getTacheById(id) == null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        tacheService.updateTache(id,dto,cin);
        return new ResponseEntity<>("la tache a été modifié avec succés", HttpStatus.OK);
    }

    @PutMapping(value = "editstatut/{id}/{statut}")
    public  ResponseEntity<String> updateStatut(@PathVariable Long id,@Valid @RequestBody String commentaire,@PathVariable String statut){
        if (tacheService.getTacheById(id) == null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.FORBIDDEN);
        tacheService.updateStatut(id,statut,commentaire);
        return new ResponseEntity<>("la tache a été modifié avec succés", HttpStatus.OK);
    }


    @DeleteMapping(value ="deletetache/{id}")
    public ResponseEntity<String> deleteRole(@PathVariable Long id ){
        if (tacheService.getTacheById(id) == null)
            return new ResponseEntity<>("la tache n'existe pas", HttpStatus.NOT_FOUND);
        tacheService.DeleteTache(id);
        return new ResponseEntity<>("la tache a été supprimé avec succés", HttpStatus.OK);
    }

}

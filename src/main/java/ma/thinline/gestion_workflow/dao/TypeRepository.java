package ma.thinline.gestion_workflow.dao;

import ma.thinline.gestion_workflow.modele.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeRepository extends JpaRepository<Type,Long> {

    List<Type> findAll();

    Type findByType(String type);

}

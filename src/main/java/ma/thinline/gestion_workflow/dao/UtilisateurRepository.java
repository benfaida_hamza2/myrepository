package ma.thinline.gestion_workflow.dao;

import ma.thinline.gestion_workflow.modele.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    Utilisateur findByUsername(String username);

    Utilisateur findByFullname(String fullname);

    Utilisateur findByCin(String cin);

}


package ma.thinline.gestion_workflow.dao;

import ma.thinline.gestion_workflow.modele.Etat;
import ma.thinline.gestion_workflow.modele.Workflow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface WorkflowRepository extends JpaRepository<Workflow,Long> {

    List<Workflow> findAll();

    List<Workflow> findWorkflowByEtat(Etat etat);

    List<Workflow> findAllByType(String type);

    @Query("select u FROM Workflow u WHERE u.workflow_id = (SELECT MAX(workflow_id) FROM Workflow)")
    Workflow findWorkflowByMaxWorkflow_id();

}

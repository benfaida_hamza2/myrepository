package ma.thinline.gestion_workflow.modele;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Workflow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long workflow_id;
    private String titre;
    private String createur;
    private String commentaire;
    private String info;

    @Temporal(value = TemporalType.DATE)
    @Column(name ="CREATED_TIME")
    private Date createdTime;

    @Temporal(value = TemporalType.DATE)
    @Column(name ="UPDATED_TIME")
    private Date updateTime;

    @OneToMany(mappedBy = "workflow")
    private List<Tache> taches;

    @ManyToOne
    @JoinColumn(name = "etat", nullable = false)
    private Etat etat;

    @ManyToOne
    @JoinColumn(name = "type", nullable = false)
    private Type type;

    public String getInfo() { return info;}

    public void setInfo(String info) { this.info = info;}

    public String getCommentaire() { return commentaire;}

    public void setCommentaire(String commentaire) { this.commentaire = commentaire;}

    public Date getCreatedTime() { return createdTime;}

    public void setCreatedTime(Date createdTime) { this.createdTime = createdTime;}

    public Long getWorkflow_id() { return workflow_id;}

    public void setWorkflow_id(Long workflow_id) { this.workflow_id = workflow_id;}

    public String getTitre() { return titre;}

    public void setTitre(String titre) { this.titre = titre;}

    public List<Tache> getTaches() { return taches;}

    public void setTaches(List<Tache> taches) { this.taches = taches;}

    public Date getUpdateTime() {return updateTime;}

    public String getCreateur() {return createur;}

    public void setCreateur(String createur) {this.createur = createur;}

    public void setUpdateTime(Date updateTime) {this.updateTime = updateTime;}

    public Etat getEtat() { return etat;}

    public void setEtat(Etat etat) { this.etat = etat;}

    public Type getType() {return type;
    }

    public void setType(Type type) {this.type = type;}
}

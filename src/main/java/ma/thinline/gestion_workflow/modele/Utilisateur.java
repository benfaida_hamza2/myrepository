package ma.thinline.gestion_workflow.modele;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

//@Data//la génération des getters et des setters...
@Entity //la génération d'une entité
@NoArgsConstructor //la génération d'un constructeur sans argument
@AllArgsConstructor //la génération d'un constructeur avec argument
@DynamicUpdate

public class Utilisateur {

    @Id //pour identifier la clé primaire
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Long user_id;
    private String fullname;
    @Column(unique = true)
    private String cin;
    @Column(unique = true)
    private String username;

    @JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
    private String password;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_workflow", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "workflow_id"))
    private  List<Workflow> workflows;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Tache> taches;

    public List<Workflow> getWorkflows() { return workflows; }

    public void setWorkflows(List<Workflow> workflows) { this.workflows = workflows; }

    public Long getUser_id() {return user_id;}

    public void setUser_id(Long user_id) {this.user_id = user_id;}

    public String getUsername() { return username;}

    public void setUsername(String username) {this.username = username;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public String getFullname() { return fullname;}

    public void setFullname(String fullname) { this.fullname = fullname;}

    public String getCin() { return cin;}

    public void setCin(String cin) { this.cin = cin;}

    public List<Role> getRoles() {return roles;}

    public void setRoles(List<Role> roles) {this.roles = roles;}

    public List<Tache> getTaches() {return taches;}

    public void setTaches(List<Tache> taches) {this.taches = taches;}
}
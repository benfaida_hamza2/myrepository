package ma.thinline.gestion_workflow.modele;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor

public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long type_id;
    private String type;

    public long getType_id() { return type_id;}

    public void setType_id(long type_id) {this.type_id = type_id;}

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

}

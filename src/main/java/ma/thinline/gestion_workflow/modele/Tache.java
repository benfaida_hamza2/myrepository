package ma.thinline.gestion_workflow.modele;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Tache {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tache_id;
    private String titre;
    @Temporal(value = TemporalType.DATE)
    @Column(name ="Date_modif")
    private Date date_modification;
    private String commentaire;
    private int ordre;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "workflow", nullable = false)
    private Workflow workflow;

    @ManyToOne
    @JoinColumn(name = "etat", nullable = false)
    private Etat etat;

    @ManyToOne
    @JoinColumn(name = "assignment", nullable = false)
    private Utilisateur user;

    public Long getTache_id() { return tache_id;}

    public void setTache_id(Long tache_id) { this.tache_id = tache_id;}

    public String getTitre() { return titre;}

    public void setTitre(String titre) { this.titre = titre;}

    public int getOrdre() { return ordre;}

    public void setOrdre(int ordre) { this.ordre = ordre;}

    public Workflow getWorkflow() { return workflow;}

    public void setWorkflow(Workflow workflow) { this.workflow = workflow;}

    public Etat getEtat() { return etat;}

    public void setEtat(Etat etat) { this.etat = etat;}

    public Date getDate_modification() { return date_modification;}

    public void setDate_modification(Date date_modification) { this.date_modification = date_modification;}

    public Utilisateur getUser() { return user;}

    public void setUser(Utilisateur user) { this.user = user;}

    public String getCommentaire() { return commentaire;}

    public void setCommentaire(String commentaire) { this.commentaire = commentaire;}
}

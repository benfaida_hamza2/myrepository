package ma.thinline.gestion_workflow.modele;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Conge extends Workflow{

    @Temporal(value = TemporalType.DATE)
    @Column(name ="Date_debut")
    private Date dateDebut;

    @Temporal(value = TemporalType.DATE)
    @Column(name ="Date_fin")
    private Date dateFin;

    @Temporal(value = TemporalType.DATE)
    @Column(name ="Date_reprise")
    private Date reprise;

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getReprise() {
        return reprise;
    }

    public void setReprise(Date reprise) {
        this.reprise = reprise;
    }
}

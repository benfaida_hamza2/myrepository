package ma.thinline.gestion_workflow.modele;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor

public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long role_id;
    private String role;

    public long getRole_id() { return role_id;}

    public void setRole_id(long role_id) { this.role_id = role_id;}

    public String getRole() { return role;}

    public void setRole(String role) { this.role = role;}
}

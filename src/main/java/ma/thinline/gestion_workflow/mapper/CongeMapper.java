package ma.thinline.gestion_workflow.mapper;

import ma.thinline.gestion_workflow.dto.CongeDto;
import ma.thinline.gestion_workflow.modele.Conge;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CongeMapper extends EntityMapper<CongeDto, Conge>{
}


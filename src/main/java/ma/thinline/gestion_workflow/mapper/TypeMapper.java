package ma.thinline.gestion_workflow.mapper;

import ma.thinline.gestion_workflow.dto.TypeDto;
import ma.thinline.gestion_workflow.modele.Type;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TypeMapper extends EntityMapper<TypeDto, Type> {
}

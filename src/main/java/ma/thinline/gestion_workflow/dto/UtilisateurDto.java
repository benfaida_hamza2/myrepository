package ma.thinline.gestion_workflow.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.thinline.gestion_workflow.modele.Tache;


import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor

public class UtilisateurDto implements Serializable {

    private Long user_id;
    private String fullname;
    private String cin;
    private String username;
    @JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
    private String password;
    private List<RoleDto> roles;
    private List<WorkflowDto> workflows;
    private List<TacheDto> taches;

    public Long getUser_id() { return user_id;}

    public void setUser_id(Long user_id) { this.user_id = user_id;}

    public String getFullname() { return fullname;}

    public void setFullname(String fullname) { this.fullname = fullname;}

    public String getCin() { return cin;}

    public void setCin(String cin) { this.cin = cin;}

    public String getUsername() { return username;}

    public void setUsername(String username) { this.username = username;}

    public String getPassword() { return password;}

    public void setPassword(String password) { this.password = password;}

    public List<RoleDto> getRoles() { return roles;}

    public void setRoles(List<RoleDto> roles) { this.roles = roles;}

    public List<WorkflowDto> getWorkflows() {return workflows;}

    public void setWorkflows(List<WorkflowDto> workflows) {this.workflows = workflows;}

    public List<TacheDto> getTaches() {return taches;}

    public void setTaches(List<TacheDto> taches) {this.taches = taches;}
}

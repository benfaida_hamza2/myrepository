package ma.thinline.gestion_workflow.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor

public class TypeDto {
    private Long type_id;
    private String type;

    public Long getType_id() { return type_id;}

    public void setType_id(Long type_id) { this.type_id = type_id;}

    public String getType() { return type;}

    public void setType(String type) { this.type = type;}
}

package ma.thinline.gestion_workflow.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor

public class CongeDto extends WorkflowDto{

    private Date dateDebut;
    private Date dateFin;
    private Date reprise;

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getReprise() {
        return reprise;
    }

    public void setReprise(Date reprise) {
        this.reprise = reprise;
    }
}

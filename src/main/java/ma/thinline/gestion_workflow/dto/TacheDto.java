package ma.thinline.gestion_workflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.thinline.gestion_workflow.modele.Etat;
import ma.thinline.gestion_workflow.modele.Utilisateur;
import ma.thinline.gestion_workflow.modele.Workflow;


import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor

public class TacheDto implements Serializable {

    private Long tache_id;
    private String titre;
    private Date date_modification;
    private int ordre;
    private EtatDto etat;
    private String commentaire;

    public Long getTache_id() {return tache_id;}

    public void setTache_id(Long tache_id) {this.tache_id = tache_id;}

    public String getTitre() {return titre;}

    public void setTitre(String titre) {this.titre = titre;}

    public int getOrdre() {return ordre;}

    public void setOrdre(int ordre) {this.ordre = ordre;}

    public EtatDto getEtat() {return etat;}

    public void setEtat(EtatDto etat) {this.etat = etat;}

    public Date getDate_modification() {return date_modification;}

    public void setDate_modification(Date date_modification) {this.date_modification = date_modification;}

    public String getCommentaire() { return commentaire;}

    public void setCommentaire(String commentaire) { this.commentaire = commentaire;}
}

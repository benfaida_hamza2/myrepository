package ma.thinline.gestion_workflow.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor

public class WorkflowDto implements Serializable {

    private Long workflow_id;
    private String titre;
    private List<TacheDto> taches;
    private Date createdTime;
    private Date updateTime;
    private TypeDto type;
    private EtatDto etat;
    private String createur;
    private String commentaire;
    private String info;

    public String getInfo() { return info;}

    public void setInfo(String info) { this.info = info;}

    public String getCommentaire() {return commentaire;}

    public void setCommentaire(String commentaire) {this.commentaire = commentaire;}

    public String getCreateur() {return createur;}

    public void setCreateur(String createur) {this.createur = createur;}

    public Date getCreatedTime() { return createdTime;}

    public void setCreatedTime(Date createdTime) { this.createdTime = createdTime;}

    public TypeDto getType() { return type;}

    public void setType(TypeDto type) {this.type = type;}

    public Date getUpdateTime() { return updateTime;}

    public void setUpdateTime(Date updateTime) { this.updateTime = updateTime;}

    public Long getWorkflow_id() {return workflow_id;}

    public void setWorkflow_id(Long workflow_id) {this.workflow_id = workflow_id;}

    public String getTitre() {return titre;}

    public void setTitre(String titre) {this.titre = titre;}

    public List<TacheDto> getTaches() {return taches;}

    public void setTaches(List<TacheDto> taches) {this.taches = taches;}

    public EtatDto getEtat() { return etat;}

    public void setEtat(EtatDto etat) { this.etat = etat;}
    
}
